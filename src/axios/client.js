import axios from "axios";

export const client = () => {
  // Create a new axios instance
  const api = axios.create({
    baseURL: "https://survey-server.morozh.pp.ua",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
    },
  });

  // Add a request interceptor to add the JWT token to the authorization header
  api.interceptors.request.use(
    config => {
      const token = localStorage.getItem("jwtToken");
      if (token) {
        config.headers.Authorization = `Bearer ${token}`;
      }
      return config;
    },
    error => Promise.reject(error)
  );

  // Add a response interceptor to refresh the JWT token if it's expired
  api.interceptors.response.use(
    response => response,
    error => {
      const originalRequest = error.config;

      if (error.response.status === 401) logout();

      // Return the original error if we can't handle it
      return Promise.reject(error);
    }
  );

  const login = async (email, password) => {
    const { data } = await api.post("/users/login", { email, password });
    // Store the JWT and refresh tokens in session storage
    localStorage.setItem("jwtToken", data.token);
    localStorage.setItem("role", data.role.toLowerCase());
    //   sessionStorage.setItem("refreshToken", data.refresh_token);
  };

  const logout = async () => {
    localStorage.removeItem("jwtToken");
    localStorage.removeItem("role");
    window.location.href = "/login";
  };

  const get = async path => {
    const response = await api.get(path);
    return response.data;
  };

  const post = async (path, data) => {
    const response = await api.post(path, data);
    return response.data;
  };

  const put = async (path, data) => {
    const response = await api.put(path, data);
    return response.data;
  };

  const del = async path => {
    const response = await api.delete(path);
    return response;
  };

  return {
    login,
    logout,
    get,
    post,
    put,
    del,
  };
};
