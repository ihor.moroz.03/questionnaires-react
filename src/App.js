import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import { Routes, Route } from "react-router-dom";
import Login from "./Login";
import Root from "./Root";
import Register from "./Register";

function App() {
  return (
    <Routes>
      <Route path="*" element={<Root />} />
      <Route path="login" element={<Login />} />
      <Route path="register" element={<Register />} />
    </Routes>
  );
}

export default App;
