import React, { useState, useRef, useEffect } from "react";
import Card from "react-bootstrap/Card";
import Container from "react-bootstrap/Container";
import Button from "react-bootstrap/Button";
import ButtonGroup from "react-bootstrap/ButtonGroup";
import Form from "react-bootstrap/Form";
import QuestinnaireView from "./QuestinnaireView";
import { Route, Routes, useNavigate } from "react-router-dom";
import Modal from "react-bootstrap/Modal";
import Error from "./Error";
import EditQuestionnaire from "./EditQuestionnaire";
import { client } from "./axios/client";

function QuestionnaireList({ role }) {
  const navigate = useNavigate();
  const [questionnaires, setQuestionnaires] = useState([
    {
      id: 0,
      name: "Опитування 1",
      description: "Опис опитування 1",
      accessible: true,
      questions: [
        {
          type: "multi",
          question: "Питання з багатьма відповідями",
          options: ["Можлива відповідь 1", "Можлива відповідь 2", "Можлива відповідь 3"],
        },
        {
          type: "text",
          question: "Питання з відкритою відповіддю",
        },
        {
          type: "single",
          question: "Питання з однією відповіддю",
          options: ["Можлива відповідь 1", "Можлива відповідь 2", "Можлива відповідь 3"],
        },
      ],
    },
    {
      id: 1,
      name: "Опитування 2",
      description: "Опис опитування 2",
      accessible: false,
      questions: [
        {
          type: "multi",
          question: "Питання з багатьма відповідями",
          options: ["Можлива відповідь 1", "Можлива відповідь 2", "Можлива відповідь 3"],
        },
      ],
    },
    {
      id: 2,
      name: "Опитування 3",
      description: "Опис опитування 3",
      accessible: true,
      questions: [
        {
          type: "multi",
          question: "Питання з багатьма відповідями",
          options: ["Можлива відповідь 1", "Можлива відповідь 2", "Можлива відповідь 3"],
        },
      ],
    },
    {
      id: 3,
      name: "Опитування 4",
      description: "Опис опитування 4",
      accessible: true,
      questions: [
        {
          type: "multi",
          question: "Питання з багатьма відповідями",
          options: ["Можлива відповідь 1", "Можлива відповідь 2", "Можлива відповідь 3"],
        },
      ],
    },
  ]);
  const [searchText, setSearchText] = useState("");
  const [showDeleteModal, setShowDeleteModal] = useState(false);
  const [error, setError] = useState(null);
  const questionnaireIdRef = useRef();

  useEffect(() => {
    client()
      .get("/surveys")
      .then(res => setQuestionnaires(res))
      .catch(err => {
        setError(err);
        console.log(err);
      });
  }, []);

  function deleteHandler(id) {
    client()
      .del(`surveys/${id}`)
      .then(
        client()
          .get("/surveys")
          .then(res => setQuestionnaires(res))
          .catch(err => {
            setError(err);
            console.log(err);
          })
      )
      .catch(err => {
        setError(err);
        console.log(err);
      });
  }

  function visibilityClickHandler(questionnaire) {
    questionnaire.accessible = !questionnaire.accessible;
    client()
      .put(`surveys/${questionnaire.id}`, questionnaire)
      .then(() =>
        setQuestionnaires(q => {
          return [...q];
        })
      )
      .catch(err => {
        setError(err);
        console.log(err);
      });
  }

  function searchTextChangeHandler(ev) {
    setSearchText(ev.target.value);
  }

  if (error !== null) return <Error code={error.code} message={error.message} />;
  return (
    <>
      <Routes>
        <Route
          index
          element={
            <Container>
              <Form.Control
                className="mb-3"
                type="search"
                placeholder="Search"
                onChange={searchTextChangeHandler}
              />
              {questionnaires
                .filter(q => q.name.includes(searchText))
                .map((questionnaire, i) => (
                  <Card key={`q${i}`} className="mb-3 shadow">
                    {questionnaire.accessible ? <></> : <Card.Header as="h6">Hidden</Card.Header>}
                    <Card.Body>
                      <Card.Title>{questionnaire.name}</Card.Title>
                      <Card.Text>{questionnaire.description}</Card.Text>
                      <ButtonGroup aria-label="Questionnaire List">
                        <Button variant="light" onClick={() => navigate(`${questionnaire.id}`)}>
                          View
                        </Button>
                        {role === "doctor" ? (
                          <>
                            <Button
                              variant="primary"
                              onClick={() => visibilityClickHandler(questionnaire)}
                            >
                              {questionnaire.accessible ? "Hide" : "Make visible"}
                            </Button>
                            <Button
                              variant="secondary"
                              onClick={() => navigate(`edit/${questionnaire.id}`)}
                            >
                              Edit
                            </Button>
                            <Button
                              variant="danger"
                              onClick={() => {
                                setShowDeleteModal(true);
                                questionnaireIdRef.current = questionnaire.id;
                              }}
                            >
                              Delete
                            </Button>
                          </>
                        ) : null}
                      </ButtonGroup>
                    </Card.Body>
                  </Card>
                ))}
            </Container>
          }
        />
        <Route path="edit/:questionnaireId" element={<EditQuestionnaire />}></Route>
        <Route
          path=":questionnaireId"
          element={
            <QuestinnaireView
              role={role}
              onDelete={deleteHandler}
              onChangeVisibility={visibilityClickHandler}
            />
          }
        ></Route>
      </Routes>
      <Modal show={showDeleteModal} onHide={() => setShowDeleteModal(false)}>
        <Modal.Header closeButton>
          <Modal.Title>Are you sure</Modal.Title>
        </Modal.Header>
        <Modal.Body>If you delete this entry, it will be gone forever</Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={() => setShowDeleteModal(false)}>
            Cancel
          </Button>
          <Button
            variant="danger"
            onClick={() => {
              deleteHandler(questionnaireIdRef.current);
              setShowDeleteModal(false);
            }}
          >
            Delete
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}

export default QuestionnaireList;
