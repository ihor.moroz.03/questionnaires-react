import React, { useState, useEffect } from "react";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import Container from "react-bootstrap/Container";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import Select from "react-select";
import Error from "./Error";
import { client } from "./axios/client";

function PatientsPage() {
  const [searchText, setSearchText] = useState("");
  const [showAssignModal, setShowAssignModal] = useState(false);
  const [patients, setPatients] = useState([]);
  const [questionnaires, setQuestionnaries] = useState([]);
  const [error, setError] = useState(null);
  const [selectedPatientId, setSelectedPatientId] = useState(null);
  const [selectedQuestionnaireId, setSelectedQuestionnaireId] = useState(null);

  useEffect(() => {
    client()
      .get(`/users/patients`)
      .then(res => setPatients(res))
      .catch(err => {
        setError(err);
        console.log(err);
      });
  }, []);

  useEffect(() => {
    if (selectedPatientId)
      client()
        .get(`/attachments/${selectedPatientId}`)
        .then(res => setQuestionnaries(res))
        .catch(err => {
          setError(err);
          console.log(err);
        });
  }, [selectedPatientId]);

  function assignHandler() {
    client()
      .post(`/attachments`, {
        user_id: selectedPatientId,
        survey_id: selectedQuestionnaireId,
      })
      .catch(err => {
        setError(err);
        console.log(err);
      });
  }

  if (error !== null) return <Error code={error.code} message={error.message} />;
  return (
    <Container className="vh-100 d-flex flex-column mb-3 mt-4">
      <Row className="mb-3">
        <Col>
          <Button onClick={() => setShowAssignModal(true)}>New assignment</Button>
        </Col>
      </Row>
      {/* <Form.Control
        className="mb-3"
        type="search"
        placeholder="Search"
        onChange={ev => setSearchText(ev.target.value)}
      />
      {patients
        .filter(p => p.first_name.includes(searchText) || p.last_name.includes(searchText))
        .map((patient, i) => (
          <Card key={`p${i}`} className="mb-3 shadow">
            <Card.Body>
              <Card.Title>{`${patient.first_name} ${patient.last_name}`}</Card.Title>
            </Card.Body>
          </Card>
        ))} */}
      <Modal show={showAssignModal} onHide={() => setShowAssignModal(false)}>
        <Modal.Header closeButton>
          <Modal.Title>Create new assignment</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Select
            className="mb-3"
            placeholder="Select patient"
            options={patients.map(p => {
              return {
                value: p,
                label: `${p.first_name} ${p.last_name}`,
              };
            })}
            onChange={({ value }) => setSelectedPatientId(value.id)}
          />
          <Select
            className="mb-3"
            placeholder="Select questionnaire"
            options={questionnaires.map(q => {
              return {
                value: q,
                label: q.name,
              };
            })}
            onChange={({ value }) => setSelectedQuestionnaireId(value.id)}
          />
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={() => setShowAssignModal(false)}>
            Cancel
          </Button>
          <Button
            onClick={() => {
              setShowAssignModal(false);
              assignHandler();
            }}
          >
            Assign
          </Button>
        </Modal.Footer>
      </Modal>
    </Container>
  );
}

export default PatientsPage;
