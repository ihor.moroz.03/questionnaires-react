import React, { useState } from "react";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import Container from "react-bootstrap/Container";
import Card from "react-bootstrap/Card";
import InputGroup from "react-bootstrap/InputGroup";
import { client } from "./axios/client";
import Error from "./Error";
import { useNavigate } from "react-router-dom";

function Register() {
  const navigate = useNavigate();
  const [error, setError] = useState(null);

  function formSubmitHandler(ev) {
    ev.preventDefault();
    client()
      .post("/users/register", {
        email: ev.target.email.value,
        first_name: ev.target.firstName.value,
        last_name: ev.target.lastName.value,
        password: ev.target.password.value,
      })
      .then(() => navigate("/"))
      .catch(err => {
        setError(err);
        console.log(err);
      });
  }

  if (error !== null) return <Error code={error.code} message={error.message} />;
  return (
    <Container>
      <Row className="vh-100 d-flex justify-content-center align-items-center">
        <Col md={10} lg={8} xs={12}>
          <div className="border border-3 border-primary"></div>

          <Card className="shadow">
            <Card.Body>
              <div className="mb-3 mt-4">
                <h2 className="fw-bold mb-2 text-uppercase">Register</h2>

                <p className=" mb-5">Please enter your details to create an account!</p>

                <Form onSubmit={formSubmitHandler}>
                  <Row className="mb-3">
                    <Form.Group as={Col} className="mb-3" controlId="firstName">
                      <Form.Label className="text-center">First Name</Form.Label>

                      <Form.Control type="text" placeholder="Enter first name" />
                    </Form.Group>

                    <Form.Group as={Col} className="mb-3" controlId="lastName">
                      <Form.Label>Last Name</Form.Label>

                      <Form.Control type="text" placeholder="Enter last name" />
                    </Form.Group>
                  </Row>

                  <Row className="mb-3">
                    <Form.Group as={Col} className="mb-3" controlId="email">
                      <Form.Label className="text-center">Email address</Form.Label>

                      <InputGroup>
                        <Form.Control type="email" placeholder="Enter email" />
                      </InputGroup>
                    </Form.Group>

                    <Form.Group as={Col} className="mb-3" controlId="password">
                      <Form.Label>Password</Form.Label>

                      <Form.Control type="password" placeholder="Password" />
                    </Form.Group>
                  </Row>

                  <div className="d-grid">
                    <Button variant="primary" type="submit">
                      Sign Up
                    </Button>
                  </div>
                </Form>

                <div className="mt-3">
                  <p className="mb-0  text-center">
                    Already have an account?{" "}
                    <a href="/login" className="text-primary fw-bold">
                      Log in
                    </a>
                  </p>
                </div>
              </div>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}

export default Register;
