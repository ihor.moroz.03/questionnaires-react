import React from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Button from "react-bootstrap/Button";
import { Routes, Route, Navigate } from "react-router-dom";
import LinkContainer from "react-router-bootstrap/LinkContainer";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import { client } from "./axios/client";
import Error from "./Error";
import QuestionnaireList from "./QuestionnaireList";

function Patient() {
  // const [error, setError] = useState(null);

  // if (error !== null) return <Error code={error.code} message={error.message} />;
  return (
    <>
      <Navbar expand="lg" className="bg-body-tertiary" sticky="top">
        <Container fluid>
          <Navbar.Brand href="/">Patient</Navbar.Brand>
          <Navbar.Toggle aria-controls="navbarScroll" />
          <Navbar.Collapse id="navbarScroll">
            <Nav className="me-auto my-2 my-lg-0" style={{ maxHeight: "100px" }} navbarScroll>
              <LinkContainer to="questionnaires">
                <Nav.Link>Questionnaires</Nav.Link>
              </LinkContainer>
              {/* <LinkContainer to="assignments">
                <Nav.Link>Assignments</Nav.Link>
              </LinkContainer> */}
            </Nav>
            <Button variant="outline-warning" onClick={() => client().logout()}>
              Log out
            </Button>
          </Navbar.Collapse>
        </Container>
      </Navbar>
      <Routes>
        <Route index element={<Navigate to="questionnaires" />} />
        <Route
          path="questionnaires/*"
          element={
            <Container className="vh-100 d-flex flex-column">
              <Row className="mt-3 mb-3"></Row>
              <QuestionnaireList role="patient"></QuestionnaireList>
            </Container>
          }
        />
        {/* <Route path="assignments" element={<></>} /> */}
        <Route path="*" element={<Error />} />
      </Routes>
    </>
  );
}

export default Patient;
