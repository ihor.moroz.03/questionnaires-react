import React, { useState } from "react";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import Container from "react-bootstrap/Container";
import Card from "react-bootstrap/Card";
import FloatingLabel from "react-bootstrap/FloatingLabel";
import { client } from "./axios/client";
import Error from "./Error";
import { Navigate } from "react-router-dom";

function Login() {
  const [error, setError] = useState(null);
  const [loggedIn, setLoggedIn] = useState(false);

  async function formSubmitHandler(ev) {
    ev.preventDefault();
    try {
      const res = await client().login(ev.target.email.value, ev.target.password.value);
      console.log(res);
      setLoggedIn(true);
    } catch (err) {
      setError(err);
      console.log(err);
    }
  }

  if (error !== null) return <Error code={error.code} message={error.response.data.message} />;
  if (loggedIn) return <Navigate to="/" />;
  return (
    <Container>
      <Row className="vh-100 d-flex justify-content-center align-items-center">
        <Col md={8} lg={6} xs={12}>
          <div className="border border-3 border-primary"></div>

          <Card className="shadow">
            <Card.Body>
              <div className="mb-3 mt-4">
                <h2 className="fw-bold mb-2 text-uppercase">Log in</h2>

                <p className=" mb-5">Please enter your email and password!</p>

                <Form className="mb-3" onSubmit={formSubmitHandler}>
                  <FloatingLabel controlId="email" label="Email" className="mb-3">
                    <Form.Control type="text" placeholder="Email" />
                  </FloatingLabel>
                  <FloatingLabel controlId="password" label="Password" className="mb-3">
                    <Form.Control type="password" placeholder="Password" />
                  </FloatingLabel>

                  {/* <div className="mb-3">
                    <p className="small">
                      <a className="text-primary" href="#!">
                        Forgot password?
                      </a>
                    </p>
                  </div> */}

                  <div className="d-grid">
                    <Button variant="primary" type="submit">
                      Login
                    </Button>
                  </div>
                </Form>

                <div className="mt-3">
                  <p className="mb-0  text-center">
                    Don't have an account?{" "}
                    <a href="/register" className="text-primary fw-bold">
                      Register
                    </a>
                  </p>
                </div>
              </div>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}

export default Login;
