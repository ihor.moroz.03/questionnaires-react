import React, { useState, useEffect } from "react";
import Card from "react-bootstrap/Card";
import Row from "react-bootstrap/Row";
import Container from "react-bootstrap/Container";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import InputGroup from "react-bootstrap/InputGroup";
import { useParams, useNavigate } from "react-router-dom";
import Error from "./Error";
import { client } from "./axios/client";
import { ArrowUp, ArrowDown, Trash, ThreeDotsVertical } from "react-bootstrap-icons";

function EditQuestionnaire() {
  const navigate = useNavigate();
  const { questionnaireId } = useParams();
  const [questionnaire, setQuestionnaire] = useState({
    name: "",
    description: "",
    accessible: false,
    questions: [
      {
        type: "text",
        question: "",
      },
    ],
  });
  const [error, setError] = useState(null);

  useEffect(() => {
    if (questionnaireId)
      client()
        .get(`/surveys/${questionnaireId}`)
        .then(res => setQuestionnaire(res))
        .catch(err => {
          setError(err);
          console.log(err);
        });
  }, []);

  function saveHandler() {
    if (!questionnaireId)
      client()
        .post("/surveys", questionnaire)
        .catch(err => {
          setError(err);
          console.log(err);
        });
    else
      client()
        .put(`/surveys/${questionnaireId}`, questionnaire)
        .catch(err => {
          setError(err);
          console.log(err);
        });

    navigate("/");
  }

  if (error !== null) return <Error code={error.code} message={error.message} />;
  return (
    <Container className="vh-100 d-flex flex-column mb-3 mt-4">
      <Row>
        <Card className="p-3 shadow">
          <Form.Group className="mb-3">
            <Form.Label>Name</Form.Label>
            <Form.Control
              type="text"
              value={questionnaire.name}
              onChange={ev =>
                setQuestionnaire(q => {
                  q.name = ev.target.value;
                  return { ...q };
                })
              }
            />
          </Form.Group>
          <Form.Group className="mb-3">
            <Form.Label>Description</Form.Label>
            <Form.Control
              as="textarea"
              rows={3}
              value={questionnaire.description}
              maxLength={1024}
              onChange={ev =>
                setQuestionnaire(q => {
                  q.description = ev.target.value;
                  return { ...q };
                })
              }
            />
          </Form.Group>
          <Form.Check
            type="switch"
            label="Visible"
            checked={questionnaire.accessible}
            onChange={ev =>
              setQuestionnaire(q => {
                q.accessible = ev.target.checked;
                return { ...q };
              })
            }
          />
        </Card>
      </Row>
      <Row className="mt-4">
        {questionnaire.questions.map((question, i) => (
          <Card key={`q${i}`} className="mb-3 p-3 shadow">
            <Container className="d-flex justify-content-end">
              {/* <Button
                className="m-1"
                onClick={() => {
                  if (i === 0) return;

                  const questions = questionnaire.questions;
                  [questions[i], questions[i - 1]] = [questions[i - 1], questions[i]];

                  setQuestionnaire({ ...questionnaire });
                }}
              >
                <ArrowUp />
              </Button> */}
              <Button
                className="m-1"
                onClick={() => {
                  if (i === 0) return;

                  const questions = questionnaire.questions;
                  [questions[i], questions[i - 1]] = [questions[i - 1], questions[i]];

                  setQuestionnaire({ ...questionnaire });
                }}
              >
                <ArrowUp />
              </Button>
              <Button
                className="m-1"
                onClick={() => {
                  if (i === questionnaire.questions.length - 1) return;

                  const questions = questionnaire.questions;
                  [questions[i], questions[i + 1]] = [questions[i + 1], questions[i]];

                  setQuestionnaire({ ...questionnaire });
                }}
              >
                <ArrowDown />
              </Button>
              <Button
                variant="danger"
                className="m-1"
                onClick={ev => {
                  questionnaire.questions.splice(i, 1);
                  setQuestionnaire({ ...questionnaire });
                }}
              >
                <Trash />
              </Button>
            </Container>
            <Form.Group className="mb-3">
              <Form.Label>Question</Form.Label>
              <Form.Control
                type="text"
                value={question.question}
                onChange={ev =>
                  setQuestionnaire(q => {
                    q.questions[i].question = ev.target.value;
                    return { ...q };
                  })
                }
              />
            </Form.Group>
            <Form.Group className={question.options ? "mb-3" : "mb-2"}>
              <Form.Label>Type</Form.Label>
              <Form.Select
                value={question.type}
                onChange={ev => {
                  setQuestionnaire(q => {
                    if (ev.target.value === "text") q.questions[i].options = null;
                    else q.questions[i].options = [];
                    q.questions[i].type = ev.target.value;
                    return { ...q };
                  });
                }}
              >
                <option value="text">Text</option>
                <option value="single">Single choice</option>
                <option value="multi">Multiple choices</option>
              </Form.Select>
            </Form.Group>
            {question.options ? (
              <Form.Group className="mb-2">
                <Form.Label>Options</Form.Label>
                {question.options.map((option, j) => (
                  <InputGroup key={`o${j}`} className="mb-2">
                    <InputGroup.Text
                      draggable
                      onDragStart={e => e.dataTransfer.setData("index", j)}
                      onDragOver={ev => ev.preventDefault()}
                      onDrop={e => {
                        e.preventDefault();

                        const oldIndex = e.dataTransfer.getData("index");
                        const newOptions = [...question.options];
                        const [draggedItem] = newOptions.splice(oldIndex, 1);
                        newOptions.splice(j, 0, draggedItem);

                        setQuestionnaire(q => {
                          q.questions[i].options = newOptions;
                          return { ...q };
                        });
                      }}
                    >
                      <ThreeDotsVertical />
                    </InputGroup.Text>
                    <Form.Control
                      type="text"
                      value={option}
                      onChange={ev =>
                        setQuestionnaire(q => {
                          if (ev.target.value.length === 0) q.questions[i].options.splice(j, 1);
                          else q.questions[i].options[j] = ev.target.value;
                          return { ...q };
                        })
                      }
                    />
                  </InputGroup>
                ))}
                <Form.Control
                  className="mb-2"
                  type="text"
                  onBlur={ev =>
                    setQuestionnaire(q => {
                      if (ev.target.value.length > 0) {
                        q.questions[i].options.push(ev.target.value);
                        ev.target.value = "";
                      }
                      return { ...q };
                    })
                  }
                  onKeyDown={ev => {
                    if (ev.key === "Enter")
                      setQuestionnaire(q => {
                        if (ev.target.value.length > 0) {
                          q.questions[i].options.push(ev.target.value);
                          ev.target.value = "";
                        }
                        return { ...q };
                      });
                  }}
                />
              </Form.Group>
            ) : null}
          </Card>
        ))}
        <Button
          variant="secondary"
          className="mb-4 "
          onClick={() => {
            questionnaire.questions.push({ type: "text", question: "" });
            setQuestionnaire({ ...questionnaire });
          }}
        >
          Add question
        </Button>
      </Row>
      <Row>
        <Button type="submit" onClick={saveHandler} className="mb-3">
          Save
        </Button>
      </Row>
    </Container>
  );
}

export default EditQuestionnaire;
