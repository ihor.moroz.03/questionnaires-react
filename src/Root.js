import React from "react";

import { Navigate } from "react-router-dom";

import Doctor from "./Doctor";
import Patient from "./Patient";

function Root() {
  const role = localStorage.getItem("role");

  if (role === "doctor") return <Doctor />;
  if (role === "patient") return <Patient />;
  return <Navigate to="/login" />;
}

export default Root;
