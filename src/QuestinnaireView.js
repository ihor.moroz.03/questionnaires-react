import React, { useState, useEffect, useRef } from "react";
import Card from "react-bootstrap/Card";
import Row from "react-bootstrap/Row";
import Container from "react-bootstrap/Container";
import Button from "react-bootstrap/Button";
import ButtonGroup from "react-bootstrap/ButtonGroup";
import Form from "react-bootstrap/Form";
import ListGroup from "react-bootstrap/ListGroup";
import FloatingLabel from "react-bootstrap/FloatingLabel";
import { useParams, useNavigate } from "react-router-dom";
import { client } from "./axios/client";
import Error from "./Error";
import Modal from "react-bootstrap/Modal";

function QuestinnaireView({ role, onDelete, onChangeVisibility }) {
  const navigate = useNavigate();
  const { questionnaireId } = useParams();
  const [questionnaire, setQuestionnaire] = useState({
    id: 0,
    name: "Опитування 1",
    description: "Опис опитування 1",
    accessible: true,
    questions: [
      {
        type: "multi",
        question: "Питання з багатьма відповідями",
        options: ["Можлива відповідь 1", "Можлива відповідь 2", "Можлива відповідь 3"],
      },
      {
        type: "text",
        question: "Питання з відкритою відповіддю",
      },
      {
        type: "single",
        question: "Питання з однією відповіддю",
        options: ["Можлива відповідь 1", "Можлива відповідь 2", "Можлива відповідь 3"],
      },
    ],
  });
  const resultId = useRef(null);
  const [forFillingIn, setForFillingIn] = useState(null);
  const [error, setError] = useState(null);
  const answersRef = useRef([]);
  const [showDeleteModal, setShowDeleteModal] = useState(false);

  useEffect(() => {
    client()
      .get(`surveys/${questionnaireId}`)
      .then(res => setQuestionnaire(res))
      .catch(err => {
        setError(err);
        console.log(err);
      });
  }, []);

  useEffect(() => {
    if (forFillingIn) {
      client()
        .get(`results/${questionnaireId}`)
        .then(res => {
          resultId.current = res.id;
        })
        .catch(err => {
          setError(err);
          console.log(err);
        });
    }
  }, [forFillingIn]);

  function cancelFillingHandler() {
    setForFillingIn(false);
    client()
      .delete(`results/${resultId.current}`)
      .catch(err => {
        setError(err);
        console.log(err);
      });
  }

  function submitHandler() {
    client()
      .post(`results`, {
        id: resultId.current,
        answers: answersRef.current.map((answer, i) => {
          return {
            type: questionnaire.questions[i].type,
            question: questionnaire.questions[i].question,
            answer: typeof answer === "string" ? [answer] : answer,
          };
        }),
      })
      .catch(err => {
        setError(err);
        console.log(err);
      })
      .finally(() => {
        navigate("/");
      });
  }

  if (error !== null) return <Error code={error.code} message={error.message} />;
  if (forFillingIn)
    return (
      <Container className="vh-100 d-flex flex-column mb-3 mt-4">
        <Row>
          {questionnaire.questions.map((question, i) => (
            <Card className="mb-3 p-3" key={`q${i}`}>
              <Card.Body>
                <Card.Title>{question.question}</Card.Title>
              </Card.Body>
              {question.type === "text" ? (
                <FloatingLabel controlId={`${i}`} label="Answer here" className="mb-3">
                  <Form.Control
                    type="text"
                    placeholder="Answer here"
                    onChange={ev => {
                      answersRef.current[i] = ev.target.value;
                    }}
                  />
                </FloatingLabel>
              ) : (
                <ListGroup>
                  {question.options.map((option, j) => (
                    <ListGroup.Item key={`q${i} o${j}`}>
                      <Form.Check
                        id={`q${i} o${j} fc`}
                        type={question.type === "single" ? "radio" : "checkbox"}
                        name={`${i}`}
                        label={option}
                        onChange={ev => {
                          if (question.type === "single") {
                            answersRef.current[i] = option;
                            return;
                          }
                          answersRef.current[i] ??= [];
                          if (ev.target.checked) {
                            answersRef.current[i].push(option);
                            return;
                          }
                          answersRef.current[i].splice(
                            answersRef.current[i].findIndex(val => val === option),
                            1
                          );
                        }}
                      />
                    </ListGroup.Item>
                  ))}
                </ListGroup>
              )}
            </Card>
          ))}
        </Row>
        <Row>
          <ButtonGroup aria-label="Questionnaire List">
            <Button className="mb-3" variant="secondary" onClick={() => cancelFillingHandler()}>
              Cancel
            </Button>
            <Button className="mb-3" onClick={submitHandler}>
              Submit
            </Button>
          </ButtonGroup>
        </Row>
      </Container>
    );
  return (
    <Container className="vh-100 d-flex flex-column mb-3 mt-4">
      <Row className="mb-3">
        <h1>{questionnaire.name}</h1>
      </Row>
      <Row className="mb-3">
        <h3>{questionnaire.description}</h3>
      </Row>
      {role === "doctor" ? (
        <Row className="mb-3">
          <Card className="p-3">
            <h5>Info</h5>
            <ListGroup>
              <ListGroup.Item variant="info">
                Total questions: {questionnaire.questions.length}
              </ListGroup.Item>
              <ListGroup.Item variant="info">2 patients submited answers</ListGroup.Item>
              <ListGroup.Item variant="info">Average filling time: 15m</ListGroup.Item>
            </ListGroup>
          </Card>
        </Row>
      ) : null}
      <Row>
        {questionnaire.questions.map((question, i) => (
          <Card className="mb-3" key={`q${i}`}>
            <Card.Body>
              <Card.Title>{question.question}</Card.Title>
            </Card.Body>
            {question.type === "multi" || question.type === "single" ? (
              <ListGroup variant="flush">
                {question.options.map((option, j) => (
                  <ListGroup.Item key={`q${i} o${j}`}>{option}</ListGroup.Item>
                ))}
              </ListGroup>
            ) : (
              <></>
            )}
          </Card>
        ))}
      </Row>
      <Row>
        {role === "doctor" ? (
          <>
            <ButtonGroup aria-label="Questionnaire List" className="mb-3">
              <Button variant="primary" onClick={() => onChangeVisibility(questionnaire)}>
                {questionnaire.accessible ? "Hide" : "Make visible"}
              </Button>
              <Button
                variant="secondary"
                onClick={() => navigate(`/questionnaires/edit/${questionnaire.id}`)}
              >
                Edit
              </Button>
              <Button variant="danger" onClick={() => setShowDeleteModal(true)}>
                Delete
              </Button>
            </ButtonGroup>
            <Modal show={showDeleteModal} onHide={() => setShowDeleteModal(false)}>
              <Modal.Header closeButton>
                <Modal.Title>Are you sure</Modal.Title>
              </Modal.Header>
              <Modal.Body>If you delete this entry, it will be gone forever</Modal.Body>
              <Modal.Footer>
                <Button variant="secondary" onClick={() => setShowDeleteModal(false)}>
                  Cancel
                </Button>
                <Button
                  variant="danger"
                  onClick={() => {
                    onDelete(questionnaireId);
                    setShowDeleteModal(false);
                  }}
                >
                  Delete
                </Button>
              </Modal.Footer>
            </Modal>
          </>
        ) : (
          <Button className="mb-3" onClick={() => setForFillingIn(true)}>
            Fill in
          </Button>
        )}
      </Row>
    </Container>
  );
}

export default QuestinnaireView;
