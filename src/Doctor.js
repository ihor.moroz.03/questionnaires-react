import React, { useState, useEffect } from "react";
import Container from "react-bootstrap/Container";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import Button from "react-bootstrap/Button";
import { Routes, Route, Navigate } from "react-router-dom";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import { LinkContainer } from "react-router-bootstrap";

import QuestionnaireList from "./QuestionnaireList";
import EditQuestionnaire from "./EditQuestionnaire";
import PatientsPage from "./PatientsPage";
import Error from "./Error";
import { client } from "./axios/client";

function Doctor() {
  const [questionnaires, setQuestionnaries] = useState([
    {
      id: 0,
      name: "Опитування 1",
      description: "Опис опитування 1",
      isVisible: true,
      questions: [
        {
          type: "multi",
          question: "Питання з багатьма відповідями",
          options: ["Можлива відповідь 1", "Можлива відповідь 2", "Можлива відповідь 3"],
        },
        {
          type: "text",
          question: "Питання з відкритою відповіддю",
        },
        {
          type: "single",
          question: "Питання з однією відповіддю",
          options: ["Можлива відповідь 1", "Можлива відповідь 2", "Можлива відповідь 3"],
        },
      ],
    },
    {
      id: 1,
      name: "Опитування 2",
      description: "Опис опитування 2",
      isVisible: false,
      questions: [
        {
          type: "multi",
          question: "Питання з багатьма відповідями",
          options: ["Можлива відповідь 1", "Можлива відповідь 2", "Можлива відповідь 3"],
        },
      ],
    },
    {
      id: 2,
      name: "Опитування 3",
      description: "Опис опитування 3",
      isVisible: true,
      questions: [
        {
          type: "multi",
          question: "Питання з багатьма відповідями",
          options: ["Можлива відповідь 1", "Можлива відповідь 2", "Можлива відповідь 3"],
        },
      ],
    },
    {
      id: 3,
      name: "Опитування 4",
      description: "Опис опитування 4",
      isVisible: true,
      questions: [
        {
          type: "multi",
          question: "Питання з багатьма відповідями",
          options: ["Можлива відповідь 1", "Можлива відповідь 2", "Можлива відповідь 3"],
        },
      ],
    },
  ]);

  useEffect(() => {
    //GET
    //   fetch("https://jsonplaceholder.typicode.com/posts", {
    //     method: "POST",
    //     body: JSON.stringify({
    //       login: ev.target["login"].value,
    //       password: ev.target["password"].value,
    //     }),
    //     headers: {
    //       "Content-type": "application/json; charset=UTF-8",
    //     },
    //   })
    //     .then(res => res.json())
    //     .then(res => {
    //       console.log(res);
    //     })
    //     .catch(err => {
    //       console.log(err.message);
    //     });
  }, []);

  return (
    <>
      <Navbar expand="lg" className="bg-body-tertiary" sticky="top">
        <Container fluid>
          <Navbar.Brand href="/">Doctor</Navbar.Brand>
          <Navbar.Toggle aria-controls="navbarScroll" />
          <Navbar.Collapse id="navbarScroll">
            <Nav className="me-auto my-2 my-lg-0" style={{ maxHeight: "100px" }} navbarScroll>
              <LinkContainer to="questionnaires">
                <Nav.Link>Questionnaires</Nav.Link>
              </LinkContainer>
              <LinkContainer to="patients">
                <Nav.Link>Patients</Nav.Link>
              </LinkContainer>
            </Nav>
            <Button variant="outline-warning" onClick={() => client().logout()}>
              Log out
            </Button>
          </Navbar.Collapse>
        </Container>
      </Navbar>
      <Routes>
        <Route index element={<Navigate to="questionnaires" />} />
        <Route
          path="questionnaires/*"
          element={
            <Container className="vh-100 d-flex flex-column">
              <Row className="mt-3 mb-3">
                <Col className="ms-3">
                  <LinkContainer to="/new">
                    <Button>New</Button>
                  </LinkContainer>
                </Col>
              </Row>
              <QuestionnaireList role="doctor"></QuestionnaireList>
            </Container>
          }
        />
        <Route path="patients" element={<PatientsPage />} />
        <Route path="new" element={<EditQuestionnaire />}></Route>
        <Route path="*" element={<Error />} />
      </Routes>
    </>
  );
}

export default Doctor;
