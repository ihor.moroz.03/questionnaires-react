import React from "react";

function Error({ code = 404, message = "The page you're looking for doesn't exist." }) {
  return (
    <div className="d-flex align-items-center justify-content-center vh-100">
      <div className="text-center">
        <h1 className="display-1 fw-bold">{code}</h1>
        <p className="fs-3">
          <span className="text-danger">Opps!</span> An error occured.
        </p>
        <p className="lead">{message}</p>
        <a href="/" className="btn btn-primary">
          Go Home
        </a>
      </div>
    </div>
  );
}

export default Error;
